# CV Ramez Tawfik
```bash
Make Sure you are in the main branch.
```
## Set up Process



- Kindly forge this repo to your profile.
- Git Clone this repo.
- After Cloning, Open it via Atom or Visual Studio.
- Redirect to the website folder using the `cd` command.
- After Words, establish you cevice terminal to install Hugo Environement.
- MacOS & Linux `brew install hugo`.
- Windows `choco install hugo -confirm`.

## Usage

- run in the terminal `hugo server`.
- then copy the local host link to your browser, usually it is [http://localhost:1313/](http://localhost:1313/) if you are not using it by another host.


### Email Redirection
----


There is a huper link to my direct email, you may need to reset up you computer configeration so the link would work.

**_Windows Operation System_**



1. Open Default Programs by clicking the ***Windows Start button***, and then clicking ***Default Programs***.
2. Or: Open the ***Control Panel*** in the ***Start Menu***, then use the search text box in the upper right corner of the Control Panel<br> screen and type ***Default Programs*** there. 
3. Hit Enter.

4. Click Set your ***Default Programs***.

5. Under Programs, click the ***Email program*** you'd like to use, and then click ***Set this program as default***.

6. Click ***OK***.

